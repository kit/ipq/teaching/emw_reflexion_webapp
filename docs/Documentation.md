## Initialization
- Aufruf setup
- Starte Schleife von main_loop

### setup()
- Create 2d Array F
- Create zs, ys arrays
	y and z values used for calculation

**Plot**
- Plotly needs: trace data, layout data
	- trace: init zs, ys, 2d F
	- Layout: 
		- Create / calc tick values in multiple of lambda
			*frequency_changed_callback()*
		- Create all lines (for gnd, boundary)
	→ Create plot

**GUI**
- Using Tweakpane Library:
	- Create GUI elements and reference them to memory / objects variables
		Some GUI elements are stored in variables to manually update them later 
		Because in two tabs the same variable is used, When values gets changed in one tab, if the tab is changed, the GUI will not get
		updated automatically
		
- Add Callback to save the current tab in a variable to determine active tab
	  *tab_select_callback()*

## Calculation

### main_loop()
- Read in parameters common for all different calculations
	  Frequency, incident angle, $\epsilon_r$
- Calc. derived parameters
	  Wave impedance, speed of light in a medium

- Depending on active tab
	- → Calc two layer
	- → Calc. parallel plate wg
	- → Calc. dielectric slab wg

- Increase time 
	- for animation
	- or not for standing image

- Update plot
	- new data in *F* 2d array
	- update lines, and shapes in plot

### Two Layer

**two_layer_calc()**
- Calculate Brewster and critical angle
	To display for used, not used in calculation

- Reset variables
	  reflection, transmission coefficient
	

Calculate the reflection and transmission coefficients 
For a two layer problem without a perfect conductor at the boundary
- Depending on the $\epsilon_r$ 
	→ Call *calc_propagating_wave()* when there is only wave propagation
	→ Call *calc_surface_wave()* when there also is attenuation


When there is a perfect conductor
- Set the reflection coefficients

- Normalize the wave impedances
   After now they are only used to calculate the amplitude of the magnetic field
   to avoid small or large amplitudes they are normalized

- Load the functions for the correct wave components
	- Functions for the incident, reflected and transmitted wave
	  *two_layer_functions()*

- Calculate all field values for all point
	  Save in *F*


**calc_propagating_wave()**
- Calculate the complex reflection and transmission coefficients

**calc_surface_wave()**
- Calculate the complex reflection and transmission 
  and attenuation coefficients

**two_layer_functions()**
 - Set the incident, reflected and transmitted wave function according to the selection on the GUI 
   and depended if there is attenuation
### Parallel Plate WG


**parallel_plate_calc()**
- Calculate the cut-off frequency of fundamental mode
	- Also save to display in GUI
- Check if there is no attenuation (above cut-off)
	- Calculate propagation constants and read in amplitudes for higher modes

- Call *parallel_plate_functions()*
	- Set anonymous functions

- Calc. all field values for all point
	  Save in *F* 


**parallel_plate_functions()**
 - Set the incident, reflected and transmitted wave function according to the selection on the GUI 
   and depended if there is attenuation