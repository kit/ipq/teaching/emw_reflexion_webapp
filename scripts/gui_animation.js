/*-----Variables and Objects--------*/
let dt = 0;
let t = 0;

let omega = 0;
let c0 = 3e8;
let k0 = 0;
let alpha_ir = 0;
let alpha_t  = 0;
let r_p = {mag : 0, angle : 0};
let t_p = {mag : 0, angle : 0};
let r_s = {mag : 0, angle : 0};
let t_s = {mag : 0, angle : 0};
let alpha = 0;
let beta = 0;
let surface_wave = false;
const E0_senk = 1;
const E0_para = 1;
const Z0 = 1;//120*Math.PI;

const EPS_R_1_MIN = 1;
const EPS_R_1_MAX = 25;
const EPS_R_2_MIN = EPS_R_1_MIN;
const EPS_R_2_MAX = 25;



let c1 = 1
let k1 = 1;
let Z1 = 1;

let c2 = 1;
let k2 = 1;
let Z2 = 1;


// https://github.com/mrdoob/stats.js/
// (function(){var script=document.createElement('script');script.onload=function(){var stats=new Stats();document.body.appendChild(stats.dom);requestAnimationFrame(function loop(){stats.update();requestAnimationFrame(loop)});};script.src='https://mrdoob.github.io/stats.js/build/stats.min.js';document.head.appendChild(script);})()
const angle_line_length = 0.04;

let annotations_visble = [{visible : true}];
let annotations_not_visible = [{visible: false}];

// For GUI
let wave_parameters = {
	frequency:6,
	angle: 60//30
};

let gui_parameters = {
	animation : true,
	component : 'Ez', // 'Ex',
	wave 	  : 'Superimposed',//'Incident', //'Superimposed',
};

let material_parameters = {
	eps_r_1 : 14,//1,
	eps_r_2 : 10,
	pec : false
};

let parallel_plate_parameters = {
	d : 0.04,
	n1 : true,
	ampl1 : 1,
	n2 : false,
	ampl2 : 1,
	n3 : true,
	ampl3 : 1,
	component : 'Hz'
};




// Memory

let f_inc = (_, j) => {return 0;}
let f_refl = (_, j) => {return 0;}
let f_trm = (_, j) => {return 0;}




const MAX_LEN = 200;
//const MAX_LEN_Y = MAX_LEN/2;
const MAX_LEN_Y_UPPER = MAX_LEN/2;
const MAX_LEN_Y_LOWER = MAX_LEN/2;
const MAX_LEN_Y = MAX_LEN_Y_UPPER + MAX_LEN_Y_LOWER;
const MAX_LEN_Z = MAX_LEN;
let y = new Array(MAX_LEN_Y);
let z = new Array(MAX_LEN_Z);
let F = new Array(MAX_LEN_Z);


// Limits for Plot
const MIN_VAL_Plot_Y = -0.05;
const MAX_VAL_Plot_Y =  0.05;
const MIN_VAL_Plot_Z = -0.05;
const MAX_VAL_Plot_Z =  0.05;


// Limits for Calculation
const MIN_VAL = -0.05;
const MAX_VAL =  0.05;

const MIN_VAL_Y = -0.05;
const MAX_VAL_Y =  0.05;

const MIN_VAL_Z = -0.05;
const MAX_VAL_Z = 0.05;


// todo Comment
const step = (MAX_VAL - MIN_VAL)/MAX_LEN;
const step_Y = (MAX_VAL_Y - MIN_VAL_Y)/MAX_LEN_Y;
const step_Z = (MAX_VAL_Z - MIN_VAL_Z)/MAX_LEN_Z;


let ticks_z = [];
let tick_text_z = [];

let ticks_y = [];
let tick_text_y = [];


let eps_slider_1;
let eps_slider_2;
let pec_checkbox;

const two_layer_tab = 0;
const parallel_plate_tab = 1;

let current_tab = two_layer_tab;

let calculated_values_two_layer = {
	brester_angle : NaN,
	critical_angle: NaN
};

let calculated_values_parallel_plate = { 

	fc : 1,
/*	two_fc : 2,
	three_fc : 3*/
	
};


function setup()
{	
	// Initialize z and y Array and F Array to two dim Array (Matrix) 
	for(let i = 0; i < MAX_LEN_Z; i++) 
	{
		z[i] = MIN_VAL_Z + i*step_Z;
		F[i] = new Array(MAX_LEN_Y);
	}

	for(let i = 0; i < MAX_LEN_Y; i++) 
	{
		y[i] = MIN_VAL_Y + i*step_Y;

	}


// #region Plotly
	/*---------Plot-------------------*/
	var trace = [{
		z:F,
		x: z,
		y:y,
		type: 'heatmap',
		
		zmax:2,
		zmin: -2,
		/*xmin : MIN_VAL_Z,
		xmax : MAX_VAL_Z,
		ymin : MIN_VAL_Y,
		ymax : MAX_VAL_Y*/
	}];


	// Generate ideale Plane vertical lines

	const dx = 0.005;
	const dy = 0.01
	let gnd_plane_lines = [{
			axref: 'x',
			ayref: 'y',
			showarrow: true,
			arrowhead: 0,
			arrowwidth: 2,
			
			x: MIN_VAL_Plot_Z,
			y: 0,
			ax: MAX_VAL_Plot_Z,
			ay: 0,
			}];
	for(let i = -9; i < 10; i++)
	{
		gnd_plane_lines.push( 
		{
			axref: 'x',
			ayref: 'y',
			showarrow: true,
			arrowhead: 0,
			arrowwidth: 2,
			
			x: i*dx,
			y: 0,
			ax: (i-1)*dx,
			ay: -dy,
			});
		annotations_visble.push({visible : true});
		annotations_not_visible.push({visible : false});
	}



	frequency_changed_callback(0); // Create axis ticks and labels

	var layout = {
		//title:"Test",
		/*width:800,
		height:800,*/
		//uirevision:"constant",
		xaxis: {
			range: [MIN_VAL_Plot_Z, MAX_VAL_Plot_Z], 
			autorange: false, 
			zeroline: false, 
			title: "z/λ",
			
			tickmode: "array",
			tickvals: ticks_z,
			ticktext: tick_text_z
		},
		yaxis: {
			range: [MIN_VAL_Plot_Y, MAX_VAL_Plot_Y], 
			autorange: false, 
			zeroline: false, 
			title: "y/λ", 

			tickmode: "array",
			tickvals: ticks_y,
			ticktext: tick_text_y
		},
		annotations: 
		[
			{ // Normal line
			x: 0,
			y: 0,
			axref: 'x',
			ax: 0,
			ayref: 'y',
			ay: angle_line_length,
			showarrow: true,
			arrowhead: 0,
			arrowwidth: 2,
			//  arrowcolor: '#636363',
			},
			{ // Angle of incident wave
			x: 0,
			y: 0,
			axref: 'x',
			ax: -angle_line_length*Math.sin(alpha_ir),
			ayref: 'y',
			ay: angle_line_length*Math.cos(alpha_ir),
			showarrow: true,
			arrowhead: 0,
			arrowwidth: 2,
			//  arrowcolor: '#636363',
			},
			{ // Angle of transmitted wave
				x: 0,
				y: 0,
				axref: 'x',
				ax: angle_line_length*Math.sin(alpha_t),
				ayref: 'y',
				ay: -angle_line_length*Math.cos(alpha_t),
				showarrow: true,
				arrowhead: 0,
				arrowwidth: 2,
				//  arrowcolor: '#636363',
			}

		].concat(gnd_plane_lines),
		shapes : // Gray rectangle as background
		[
			{
				type: 'rect',
				xref: 'paper',
				yref: 'y',
				x0: 0, 
				y0: 0,
				x1: 1,
				y1: MIN_VAL_Plot_Z,
				fillcolor: 'rgb(169,169,169)',
				opacity: 0.2,
				line: {width: 0}
			},
			{ // dashed line at border between dielectrics
				// https://plotly.com/javascript/reference/layout/shapes/#layout-shapes
				type: 'line',
				x0: MIN_VAL_Plot_Z,
				y0: 0,
				x1: MAX_VAL_Plot_Z,
				y1: 0,
				line: {
				  //color: 'rgb(50, 171, 96)',
				 // width: 4,
				  dash: 'dash'
				}
			},
			{
				type: 'line',
				x0: MIN_VAL_Plot_Z,
				y0: parallel_plate_parameters.d,
				x1: MAX_VAL_Plot_Z,
				y1: parallel_plate_parameters.d

			}
		]
		
	}; // end layout


	Plotly.newPlot('myDiv', trace, layout, {responsive:true}); //TODO myDiv umbenennen
// #endregion

/******************************************************************************************** */

// #region GUI
	/*---------GUI-------------------*/
	const pane = new Tweakpane.Pane({
		title: 'Parameters',
		container: document.getElementById('itemSideLeftMiddle'),
	});

	let frequency_slider = pane.addInput(wave_parameters, 'frequency',
	{
		min : 1,
		max : 15,
		step : 1
	}
	
	);
	frequency_slider.on('change', frequency_changed_callback);
	pane.addInput(gui_parameters, "animation");

	const tab = pane.addTab({
		pages: 
		[
			{title: 'Two Layer'},
			{title: 'Parallel Plate'}
		]

	});

	tab.on('select', tab_select_callback);

	const parameter_folder = tab.pages[0].addFolder({
		title:'Incident Wave Parameters'
	});

	parameter_folder.addInput(wave_parameters, 'angle',
		{
			min : 0,
			max : 90,
			step : .5
		}
	);

	/////////////////////////////////////////////  Two Layer
	const medium_folder = tab.pages[0].addFolder({
		title : 'Medium Parameters'
	});

	eps_slider_1 = medium_folder.addInput(material_parameters, "eps_r_1", 
		{		
			min : EPS_R_1_MIN,
			max : EPS_R_1_MAX,
			step: .5
		}
	);

	eps_slider_2 = medium_folder.addInput(material_parameters, "eps_r_2", 
		{		
			min : EPS_R_2_MIN,
			max : EPS_R_2_MAX,
			step: 1
		}
	);
	pec_checkbox = medium_folder.addInput(material_parameters, "pec");
	pec_checkbox.on('change', pec_callback);
	//medium_folder.on('change', moded_changed_callback);


	const gui_folder = tab.pages[0].addFolder({
		title : 'GUI Parameter' //TODO Besseren Namen überlegen
	});

	gui_folder.addInput(gui_parameters, "component",{
		options: 
		{
			'Ex (perpendicular)' : 'Ex',
			'Ey (parallel)': 'Ey',
			'Ez (parallel)' : 'Ez',
			'Hx (parallel)' : 'Hx',
			'Hy (perpendicular)': 'Hy',
			'Hz (perpendicular)' : 'Hz'
		}
	});

	gui_folder.addInput(gui_parameters, "wave",{
		options: 
		{
			Superimposed : 'Superimposed',
			Incident : 'Incident',
			Reflected : 'Reflected'
		}
	});

	const calc_values_folder = tab.pages[0].addFolder({
		title: "Angles"
	});

	calc_values_folder.addMonitor(calculated_values_two_layer, 'brester_angle');
	calc_values_folder.addMonitor(calculated_values_two_layer, 'critical_angle');

	///////////////////////////////////////////// Parallel Plate
	const parallel_plate_page = tab.pages[1];
	parallel_plate_page.addInput(parallel_plate_parameters, 'd', {
		min: 0.01,
		max: .05,
		step : 0.005
	});
	parallel_plate_page.addInput(parallel_plate_parameters, 'n1');
	parallel_plate_page.addInput(parallel_plate_parameters, 'ampl1', 
	{		
		min : -2,
		max : 2,
		step: 0.1
	});

	parallel_plate_page.addInput(parallel_plate_parameters, 'n2');
	parallel_plate_page.addInput(parallel_plate_parameters, 'ampl2', 
	{		
		min : -2,
		max : 2,
		step: 0.1
	});

	parallel_plate_page.addInput(parallel_plate_parameters, 'n3');
	parallel_plate_page.addInput(parallel_plate_parameters, 'ampl3', 
	{		
		min : -2,
		max : 2,
		step: 0.1
	});

	parallel_plate_page.addInput(parallel_plate_parameters, "component",{
		options: 
		{
			'Ez (E Wave)' : 'Ez',
			'Hz (H Wave)' : 'Hz'
		}
	});

	parallel_plate_page.addMonitor(calculated_values_parallel_plate, 'fc');


	// #endregion
}

/*---------------------- Callbacks------------------*/

// #region Callbacks

function tab_select_callback(ev)
{
	current_tab = ev.index;
	// slider dont automaticly update when tab is changed
	eps_slider_1.refresh();
	eps_slider_2.refresh();

}

function frequency_changed_callback(ev)
{
	let lambda = c0 / (wave_parameters.frequency*1e9);

	// z-axis
	let MAX_Pos_Z = Math.floor(MAX_VAL_Plot_Z / lambda);
	let MAX_Neg_Z = Math.floor( Math.abs(MIN_VAL_Plot_Z) / lambda );
	
	let arr = Array.from({length: MAX_Pos_Z + MAX_Neg_Z +1}, (_, i) => i - MAX_Neg_Z);
	ticks_z = arr.map(x => x*lambda);
	tick_text_z = arr;

	// y-axis
	let MAX_Pos_Y = Math.floor(MAX_VAL_Plot_Y / lambda);
	let MAX_Neg_Y = Math.floor( Math.abs(MIN_VAL_Plot_Y) / lambda );
	
	arr = Array.from({length: MAX_Pos_Y + MAX_Neg_Y +1}, (_, i) => i - MAX_Neg_Y);
	ticks_y = arr.map(x => x*lambda);
	tick_text_y = arr;


}

function pec_callback(ev)
{
	eps_slider_1.refresh();
	if(material_parameters.pec == true)
	{
		eps_slider_2.disabled = true;
	}
	else
	{
		eps_slider_2.disabled = false;
	}
}
// #endregion

/*--------------------------------Main Loop--------------*/
// #region Two_Layer
function two_layer_functions()
{


	f_inc = (_, y) => {return 0;}
	f_refl = (_, y) => {return 0;}

	
	const xyz = gui_parameters.component;
	const w = gui_parameters.wave;
	if(xyz == 'Ex')
	{	
		if( w == 'Incident' || w == 'Superimposed' )
		{
			f_inc = (z,y) => { return  Math.cos(  omega*t + k1*Math.cos(alpha_ir)*y - k1*Math.sin(alpha_ir)*z   )   ;}
		}
		if( w == 'Reflected' || w == 'Superimposed' )
		{
			f_refl = (z,y) => { return r_s.mag*Math.cos(  omega*t - k1*Math.cos(alpha_ir)*y - k1*Math.sin(alpha_ir)*z +r_s.angle  )   ;}
		}
		if(!surface_wave)
		{
			f_trm = (z,y) => {return t_s.mag* Math.cos(  omega*t + k2*Math.cos(alpha_t)*y - k2*Math.sin(alpha_t)*z  + t_s.angle )  ;}
		}
		else
		{
			f_trm = (z,y) => {return t_s.mag* Math.exp(alpha*y)*Math.cos(omega*t - beta*z + t_s.angle) ;  }
		}
	}
	else if(xyz == 'Ey')
	{	
		if( w == 'Incident' || w == 'Superimposed' )
		{
			f_inc = (z,y) => { return   Math.sin(alpha_ir)*Math.cos(  omega*t + k1*Math.cos(alpha_ir)*y - k1*Math.sin(alpha_ir)*z   )   ;}
		}
		if( w == 'Reflected' || w == 'Superimposed' )
		{
			f_refl = (z,y) => { return   r_p.mag*Math.sin(alpha_ir)*Math.cos(  omega*t - k1*Math.cos(alpha_ir)*y - k1*Math.sin(alpha_ir)*z   + r_p.angle ) ;}
		}
		if(!surface_wave)
		{
			f_trm = (z,y) => {return   t_p.mag* Math.sin(alpha_ir)*Math.cos(  omega*t + k2*Math.cos(alpha_t)*y - k2*Math.sin(alpha_t)*z +  t_p.angle);}
		}
		else
		{
			f_trm = (z,y) => {return   t_p.mag* Math.exp(alpha*y)* beta/k2* Math.cos(  omega*t  -beta*z  + t_p.angle );}
		}
	}
	else if(xyz == 'Ez')
	{	
		if( w == 'Incident' || w == 'Superimposed' )
		{
			f_inc = (z,y) => { return  Math.cos(alpha_ir)*Math.cos(  omega*t + k1*Math.cos(alpha_ir)*y - k1*Math.sin(alpha_ir)*z   )    ;}
		}
		if( w == 'Reflected' || w == 'Superimposed' )
		{
			f_refl = (z,y) => { return  -r_p.mag*Math.cos(alpha_ir)*Math.cos(  omega*t - k1*Math.cos(alpha_ir)*y - k1*Math.sin(alpha_ir)*z +  r_p.angle )   ;}
		}
		
		if(!surface_wave)
		{
			f_trm = (z,y) => {return  t_p.mag* Math.cos(alpha_ir)*Math.cos(  omega*t + k2*Math.cos(alpha_t)*y - k2*Math.sin(alpha_t)*z + t_p.angle  ) ;}
		}
		else
		{
			f_trm = (z,y) => {return   t_p.mag* Math.exp(alpha*y)* alpha/k2* Math.cos(  omega*t  -beta*z  + Math.PI/2  + t_p.angle );}
		}
	}

//////////////////////////////////////////////////////////////////////////////////////////////////

if(xyz == 'Hx')
	{	
		if( w == 'Incident' || w == 'Superimposed' )
		{
			f_inc = (z,y) => { return -E0_para/Z1*Math.cos(omega*t + k1*Math.cos(alpha_ir)*y - k1*Math.sin(alpha_ir)*z)   ;}
		}
		if( w == 'Reflected' || w == 'Superimposed' )
		{
			f_refl = (z,y) => { return -E0_para*r_p.mag/Z1*Math.cos(omega*t - k1*Math.cos(alpha_ir)*y - k1*Math.sin(alpha_ir)*z + r_p.angle)  ;}
		}
		if(!surface_wave)
		{
			f_trm = (z,y) => {      return -E0_para*t_p.mag/Z2 * Math.cos(omega*t + k2*Math.cos(alpha_t)*y - k2*Math.sin(alpha_t)*z + t_p.angle) ;}
		}
		else
		{
			f_trm = (z,y) => {return   -t_p.mag* Math.exp(alpha*y)*       Math.cos(  omega*t  -beta*z  + t_p.angle );}
		}
	}
	else if(xyz == 'Hy')
	{	
		if( w == 'Incident' || w == 'Superimposed' )
		{
			f_inc = (z,y) => { return    E0_senk/Z1*Math.sin(alpha_ir)*Math.cos(omega*t + k1*Math.cos(alpha_ir)*y - k1*Math.sin(alpha_ir)*z) ;}
		}
		if( w == 'Reflected' || w == 'Superimposed' )
		{
			f_refl = (z,y) => { return   E0_senk*r_s.mag/Z1*Math.sin(alpha_ir)*Math.cos(omega*t - k1*Math.cos(alpha_ir)*y - k1*Math.sin(alpha_ir)*z   + r_s.angle) ;}
		}
		if(!surface_wave)
		{
			f_trm = (z,y) => {     return    E0_senk*t_s.mag/Z2*Math.sin(alpha_ir)*Math.cos(omega*t + k2*Math.cos(alpha_t)*y - k2*Math.sin(alpha_t)*z  + t_s.angle) ;}
		}
		else
		{
			f_trm = (z,y) => {return   E0_senk/Z2 * t_p.mag * beta/k2 *  Math.exp(alpha*y)* Math.cos(  omega*t  -beta*z  + t_p.angle );}
		}
	}
	else if(xyz == 'Hz')
	{	
		if( w == 'Incident' || w == 'Superimposed' )
		{
			f_inc = (z,y) => { return   E0_senk/Z1*Math.cos(alpha_ir)*Math.cos(omega*t + k1*Math.cos(alpha_ir)*y - k1*Math.sin(alpha_ir)*z)   ;}
		}
		if( w == 'Reflected' || w == 'Superimposed' )
		{
			f_refl = (z,y) => { return - E0_senk*r_s.mag/Z1*Math.cos(alpha_ir)*Math.cos(omega*t - k1*Math.cos(alpha_ir)*y - k1*Math.sin(alpha_ir)*z  + r_s.angle);}
		}
		if(!surface_wave)
		{
			f_trm = (z,y) => {      return   E0_senk*t_s.mag/Z2*Math.cos(alpha_ir)*Math.cos(omega*t + k2*Math.cos(alpha_t)*y - k2*Math.sin(alpha_t)*z  + t_s.angle)   ;}
		}
		else
		{
			f_trm = (z,y) => {return   E0_senk/Z2 * t_p.mag * alpha/k2 *  Math.exp(alpha*y)* Math.cos(  omega*t  -beta*z +  Math.PI/2 + t_p.angle );}
		}
	}

}


function calc_propagating_wave()
{
	// Transmitted Angle
	alpha_t = Math.asin ( k1 / k2 * Math.sin(alpha_ir));

	// reflection and transmission for PERPENDICULAR polarisation
	r_s.mag = ( Z2*Math.cos(alpha_ir) - Z1*Math.cos(alpha_t) ) / (Z2*Math.cos(alpha_ir) + Z1*Math.cos(alpha_t));
	t_s.mag = 1 + r_s.mag;
	// reflection and transmission for PARALLEL polarisation
	r_p.mag = (Z1*Math.cos(alpha_ir) - Z2*Math.cos(alpha_t)) / ( Z1*Math.cos(alpha_ir) + Z2*Math.cos(alpha_t) );
	t_p.mag = Z2/Z1*(1 + r_p.mag);
}

function calc_surface_wave()
{
	let re = 2*Z1*Z2*(Math.cos(alpha_ir))**2;
	let im =  -2* alpha/k2 * Z2**2 *Math.cos(alpha_ir);
	t_p.angle = argument(re, im);
	t_p.mag = Math.sqrt(re**2+ im**2)/( (alpha/k2*Z2)**2+  (Z1*Math.cos(alpha_ir))**2 );


	re = -1*(alpha/k2 * Z2)**2 + (Z1*Math.cos(alpha_ir))**2;
	im = -2*alpha/k2 *Z1* Z2*Math.cos(alpha_ir); // VZ + oder - ?
	r_p.angle = argument(re, im);
	r_p.mag = 1; //is eq. to 1; see Pozar; //  Math.sqrt(re**2+ im**2) / ((alpha/k2 * Z2)**2 + (Z1*Math.cos(alpha_ir))**2);


	re = 2*(Z2*Math.cos(alpha_ir))**2;
	im = -2*Z1*Z2*alpha/k2*Math.cos(alpha_ir);
	t_s.angle = argument(re, im);
	t_s.mag = Math.sqrt(re**2+ im**2) / ( (Z2*Math.cos(alpha_ir))**2 + (Z1*alpha/k2)**2  );

	re = (Z2*Math.cos(alpha_ir))**2 - (Z1*alpha/k2)**2;
	im = -2*Z1*Z2*alpha/k2*Math.cos(alpha_ir);
	r_s.angle = argument(re, im);
	r_s.mag = 1;

}
function two_layer_calc()
{
	if(eps_r_1 >= eps_r_2)
	{
		calculated_values_two_layer.critical_angle = 180/Math.PI * Math.asin(Math.sqrt(eps_r_2/eps_r_1));
	}
	else 
	{
		calculated_values_two_layer.critical_angle = NaN;
	}
	calculated_values_two_layer.brester_angle =  180/Math.PI * Math.atan(Math.sqrt(eps_r_2/eps_r_1));

	// reset variables
	surface_wave = false;

	t_p.angle = 0;
	t_p.mag = 0;

	r_p.angle = 0;
	r_p.mag = 0;

	t_s.angle = 0;
	t_s.mag = 0;

	r_s.angle = 0;
	r_s.mag = 0;

	if(!material_parameters.pec) // if non conductive
	{
		if(eps_r_1 <= eps_r_2)
		{
			calc_propagating_wave();
		}
		else // eps_r_1 > eps_r_2 ==> surface wave, attenuation
		{

			beta = k1*Math.sin(alpha_ir);

			const alpha_ = beta**2 - k2**2;
			if(alpha_ >= 0) // angle > angle crit
			{
				surface_wave = true;
				alpha = Math.sqrt(alpha_);
				calc_surface_wave();
			}
			else // angle < angle crit
			{
				calc_propagating_wave();
			}

		}

	}
	else // if perfect conductor
	{
			r_s.mag = -1;
			r_p.mag = 1;
	}


	//Normalize Wave Impedance 
	/*
	 Z1, Z2 could be smaller than 1 --> 1/Z1 is bigger than 1 --> Amplitude of H becomes big
	 Alternative would be to change Z0 (currently 1) to a higher value
	 In this approach the incident H wave will have an amplitude of 1
	*/
	Z2 = Z2/Z1;
	Z1 = 1; // Z1 / Z1

	two_layer_functions(); // update calculation function; dependeted on decission made above

	

	// Calculation

	for(let i = 0; i < MAX_LEN_Z; i++) 
	{
		for(let j = 0; j < MAX_LEN_Y_LOWER; j++) 
		{
			if(!material_parameters.pec )
			{
				F[j][i] = f_trm(z[i],y[j]);
			}
			else
			{
					F[j][i] = null; // don't show in plot
			}
		}
		for(let j = MAX_LEN_Y_LOWER; j < MAX_LEN_Y; j++) 
		{
			F[j][i] = f_inc(z[i],y[j]) + f_refl(z[i],y[j]) ;
		}
	}
}
// #endregion
 
// #region Parallel_Plate_WG
function parallel_plate_functions()
{
	
	f_inc = (_, y) => {return 0;}
	f_refl = (_, y) => {return 0;}

	
	const xyz = parallel_plate_parameters.component;
	if(xyz == 'Ez')
	{		
		if(!parallel_plate_aperiodic_attenuation)
		{
			f_inc = (z,y,beta_z, beta_c) => {return  Math.sin(beta_c*y)*Math.cos(  omega*t - beta_z*z  ) ;}
		}
		else
		{
			//                                    avoid amplifying the amplitude for z < 0 ==> set new starting point at MIN_VAL_Z
			f_inc = (z,y) => {return   Math.sin(beta_c*y)*Math.exp(-alpha*(z - MIN_VAL_Z))*Math.cos(  omega*t);}
		}
	}

//////////////////////////////////////////////////////////////////////////////////////////////////
	if(xyz == 'Hz')
	{	
		if(!parallel_plate_aperiodic_attenuation)
		{
			f_inc = (z,y,beta_z,beta_c) => {return  Math.cos(beta_c*y)*Math.cos(  omega*t - beta_z*z  ) ;}
		}
		else
		{
			//                                    avoid amplifying the amplitude for z < 0 ==> set new starting point at MIN_VAL_Z
			f_inc = (z,y) => {return   Math.cos(beta_c*y)*Math.exp(-alpha*(z - MIN_VAL_Z))*Math.cos(  omega*t);}
		}
	}

}

let f_c;
let beta_c;
let beta_z;
let beta_0;
let parallel_plate_aperiodic_attenuation;
function parallel_plate_calc()
{
	const d = parallel_plate_parameters.d;
	const v = c0;
	const f = wave_parameters.frequency * 1e9;
	f_c = v/(2*d);
		calculated_values_parallel_plate.fc  = f_c/1e9;
	beta_c = Math.PI/d;
	beta_0 = omega/v;
	parallel_plate_aperiodic_attenuation = false;
	let beta_z_vector = [];
	let beta_c_vector = [];
	let ampl_vector = [];

	if(beta_0**2-beta_c**2 < 0) // aperiodic attenuation
	{
		alpha = Math.sqrt(beta_c**2 - beta_0**2);
		parallel_plate_aperiodic_attenuation = true;
	}
	else //propagation
	{
		//beta_z = Math.sqrt(beta_0**2 - beta_c**2);
		if(parallel_plate_parameters.n1 == true)
		{
			if(f >= 1*f_c)
			{
				beta_z_vector.push( Math.sqrt(beta_0**2 - beta_c**2 + 1e-3)   );
				beta_c_vector.push(1*beta_c);
				ampl_vector.push(parallel_plate_parameters.ampl1);
			}
		}
		if(parallel_plate_parameters.n2 == true)
		{
			if( f >= 2*f_c)
			{
				beta_z_vector.push( Math.sqrt(beta_0**2 - (2*beta_c)**2 + 1e-3)   );
				beta_c_vector.push(2*beta_c);
				ampl_vector.push(parallel_plate_parameters.ampl2);
			}
		}
		if(parallel_plate_parameters.n3 == true)
		{
			if( f >= 3*f_c)
			{
				beta_z_vector.push( Math.sqrt(beta_0**2 - (3*beta_c)**2 + 1e-3)   );
				beta_c_vector.push(3*beta_c);
				ampl_vector.push(parallel_plate_parameters.ampl3);
			}
		}
	}
	parallel_plate_functions();

	
	const max_len_Y_middle = parallel_plate_parameters.d / MAX_VAL_Y*MAX_LEN_Y_UPPER + MAX_LEN_Y_LOWER; //index where second plate sits
	

	
	// Calculation

	for(let i = 0; i < MAX_LEN_Z; i++) 
	{
		for(let j = 0; j < MAX_LEN_Y_LOWER; j++) 
		{
			F[j][i] = null; // don't show in plot	
		}
		for(let j = MAX_LEN_Y_LOWER; j < max_len_Y_middle; j++) 
		{
		
			if(parallel_plate_aperiodic_attenuation)
			{
				F[j][i] = f_inc(z[i],y[j]);
			}
			else
			{
				F[j][i] = 0;
				for(let m = 0; m < beta_z_vector.length; m++)
				{
					F[j][i] += ampl_vector[m]*f_inc(z[i],y[j], beta_z_vector[m], beta_c_vector[m]); 
				}
			}
		}
		for(let j = max_len_Y_middle; j < MAX_LEN_Y; j++) 
		{
			F[j][i] = null; //area above second conductor; don't show in plot
		}
	}
}

// #endregion


let eps_r_1;
let eps_r_2;

function main_loop() 
{
// Read in parameter
	dt = 1/(1e9*wave_parameters.frequency*20);
	omega = 2*Math.PI*wave_parameters.frequency*1e9;
	alpha_ir = wave_parameters.angle/180*Math.PI;
	k0 = omega/c0;

	eps_r_1 = material_parameters.eps_r_1;
	eps_r_2 = material_parameters.eps_r_2;
	
// Calculate derived parameters	
	const mu_r = 1;
	c1 = c0 /Math.sqrt(eps_r_1*mu_r); // in medium 1  mu_r = 1
	k1 = omega/c1;
	Z1 =Z0* Math.sqrt(mu_r / eps_r_1);

	c2 = c0 /Math.sqrt(eps_r_2*mu_r);
	k2 = omega / c2;
	Z2 = Z0 * Math.sqrt(mu_r / eps_r_2);


	if(current_tab == two_layer_tab)
	{
		two_layer_calc();
	}
	else if(current_tab == parallel_plate_tab)
	{
		parallel_plate_calc();
	}



	
	if(gui_parameters.animation)
	{
		t += dt;
	}

	
// Animation
	 


	Plotly.animate('myDiv', 
	{
		data: [{z: F, x:z, y:y}	],
		layout :{
			xaxis:
			{
				tickvals: ticks_z,
				ticktext: tick_text_z
			},
			yaxis:
			{
				tickvals: ticks_y,
				ticktext: tick_text_y
			},
			annotations:  
			[
				{visible: current_tab == two_layer_tab}, // Don't change the line normal to the surface
				{ax: -angle_line_length*Math.sin(alpha_ir), ay: angle_line_length*Math.cos(alpha_ir),
					visible: current_tab == two_layer_tab},
				{ax: angle_line_length*Math.sin(alpha_t),  ay: -angle_line_length*Math.cos(alpha_t),
					visible: material_parameters.pec && (current_tab == two_layer_tab) || surface_wave ? false : true}
				
			].concat(material_parameters.pec ? annotations_visble : annotations_not_visible),
			shapes : [
				{visible : material_parameters.pec}, // hide gray rectangle when pec is not selected
				{visible : !material_parameters.pec}, // hide dashed line if pec is selected
				{visible : current_tab == parallel_plate_tab , y0: parallel_plate_parameters.d,
					y1: parallel_plate_parameters.d} // hide and move line for second boundary
			]
		}
	}, 
	{
		transition: 
		{
			duration: 0
		},
		frame: 
		{
			duration: 0,
			redraw: true
		}
	}
	);

}


function argument(r, i)
{
	if(r == 0)
	{
		if(i >= 0)
			return Math.PI/2;
		else
			return -Math.PI/2;
	}
	let angle = Math.atan(i/r);

	if(r > 0)
		return angle;
	if( i >= 0) //&& r <0 
		return angle + Math.PI;
	// i < 0
	return angle-Math.PI;
		
}




let previous; // Save last time since update_loop_called was called
const fps = 7; // Frame rate

setup(); // Call setup to initialize GUI, etc.
requestAnimationFrame(update_loop_caller); // Initial call for loop

function update_loop_caller (timestamp)
{
	const interval = 1000/fps;

	if (previous === undefined) 
	{
        previous = timestamp;
	}
	  
    const delta = timestamp - previous;
     
    if (delta > interval) 
	{
		main_loop(); // Call calculate
		
		previous = timestamp;
	
	}
	requestAnimationFrame(update_loop_caller);
}
